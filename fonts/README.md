﻿# Pepper&Carrot fonts
Open fonts used in the webcomic [Pepper&Carrot](https://www.peppercarrot.com)

Fonts are sorted by script:

* Arabic (عربي)
* Bengali
* Cyrilic (українська, Pусский)
* Farsi (فارسی)
* Greek (ελληνικά)
* Hebrew (עִבְרִית)
* Japanese (日本語)
* Latin (Català, Deutsch, English, Español, Esperanto, Français, Italiano, Mexicano, Nederlands, Polski, Português)
* Sinhalese (සිංහල)
* Vietnamese (Tiếng Việt)
* Chinese simplified (简体中文)
* Korean (한국어)

## Installation

On Arch Linux and derivates, you can install all of these fonts at once by installing [peppercarrot-fonts](https://aur.archlinux.org/packages/peppercarrot-fonts/) from the AUR.

On other platforms, you may have to install the fonts you want to use manually. How to do that depends on your operating system. Often you can either use a graphical font manager, or copy the font files to a special directory. Please search instructions for your specific platform.

## Modifications/Derivations

Modification or derivation can be done with FontForge. FontForge sources files (sfd) can be saved in a subdirectory "sources".

## License, author, details and links

**Note:** Every font or contribution to modify something in this project **must** be compatible with the CC-By license.

For example, these are allowed: public domain fonts, GNU/GPL fonts, CC-0 fonts, CC-By fonts, OFL (SIL Open Font License) fonts. These aren't: fonts that are *free for personal use*, fonts with *all rights reserved*.

When adding a new font, a `font.LICENSE` and `font.COPYRIGHT` file is required. The COPYRIGHT file can be copied from `template.COPYRIGHT` and filled in. Remove properties that are not applicable. Only fill in "Reserved font name" for fonts licensed under an OFL license!

## Special thanks

A big thanks to the creators of all fonts in this directory, and also a special thanks to the website owner and maintainer of [openfontlibrary.org](http://openfontlibrary.org) for creating the best and safest place to find open-source fonts.
